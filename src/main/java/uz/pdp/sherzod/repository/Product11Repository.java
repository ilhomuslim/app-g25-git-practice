package uz.pdp.sherzod.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.sherzod.entity.Product11;

import java.util.UUID;

public interface Product11Repository extends JpaRepository<Product11, UUID> {
}
