package uz.pdp.xurshid.repository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "Xurshid")
public class collegeRepository {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String first_name;

    private String last_name;
}
