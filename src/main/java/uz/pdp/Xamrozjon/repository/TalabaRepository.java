package uz.pdp.Xamrozjon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.Xamrozjon.entity.Talaba;

import java.util.UUID;

public interface TalabaRepository extends JpaRepository<Talaba, UUID> {

}
