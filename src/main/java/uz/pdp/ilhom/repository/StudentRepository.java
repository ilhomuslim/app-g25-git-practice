package uz.pdp.ilhom.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.ilhom.entity.Student;

import java.util.UUID;

public interface StudentRepository extends JpaRepository<Student, UUID> {
}
