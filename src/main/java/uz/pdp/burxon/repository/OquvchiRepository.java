package uz.pdp.burxon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.burxon.entity.Oquvchi;

import java.util.UUID;

public interface OquvchiRepository extends JpaRepository<Oquvchi, UUID> {


}
