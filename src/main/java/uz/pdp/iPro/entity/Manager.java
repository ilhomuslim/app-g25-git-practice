package uz.pdp.iPro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "Jasur")

public class Manager {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private UUID id;

    private String first_name;

    private String last_name;

}
